package web

import java.util

import config.Ctx
import Ctx._
import house_price.HousePricePredictor.PricePaid
import house_price.{HousePricePredictor, HousePriceRequest, Postcode}
import spark.{Request, Response, Route}

import scala.collection.JavaConversions._

/**
  * Created by Riz
  */
object PostCodeCtl extends Route{
  override def handle(request: Request, response: Response): AnyRef = {
    val postCode = request.queryParams("pc")
    if(postCode != null){
      return  get(request,response, postCode)
    }

    throw new RuntimeException("Unable to find page")
  }


  def get(request: Request, response: Response, postcode: String): String ={
    val model = new util.HashMap[String, AnyRef]()
    model.put("postcode", postcode)


    if(Postcode.checkPostcodeExists(postcode)){
      val result = HousePricePredictor.predictHousePrice(new HousePriceRequest(postcode))

      val pricesPaid = result.pricesPaid
                              .sortBy(_.date)
                              .takeRight(10).
                              reverse.map(pp =>
                                new PricePaid(
                                  price = pp.price, date = pp.date.replaceAll("00:00:00.0", ""),
                                  paon = pp.paon, street = pp.street, postcode= pp.postcode))


      val ppJava: java.util.List[PricePaid] = pricesPaid


      model.put("estimatedPrice", result.estimatedPrice.toString)
      model.put("pricesPaid",ppJava)
    }
    else {
      model.put("error", s"Invalid postcode ${postcode}")
    }
    view("postcode_info", model)
  }
}
