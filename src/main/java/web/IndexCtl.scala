package web

import config.Ctx
import Ctx._
import spark.{Request, Response, Route}

/**
  * Created by Riz
  */
object IndexCtl extends Route{
  override def handle(request: Request, response: Response): AnyRef = {
    view("index")
  }
}
