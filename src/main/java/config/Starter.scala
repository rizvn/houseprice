package config

import spark.Spark._
import web.{IndexCtl, PostCodeCtl}

/**
  * Created by Riz
  */
object Starter {

  def main(arg: Array[String]) = {

    //on heroku first arg is port
    if(arg.size > 0){
      port(arg(0).toInt)
    }
    staticFileLocation("/static")

    get("/",         IndexCtl)
    get("/postcode", PostCodeCtl)
  }

}

