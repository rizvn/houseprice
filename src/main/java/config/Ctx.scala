package config

import java.io.{FileInputStream, StringWriter}
import java.nio.charset.StandardCharsets
import java.util
import java.util.Properties
import javax.sql.DataSource

import com.mitchellbosecke.pebble.PebbleEngine
import com.mitchellbosecke.pebble.loader.ClasspathLoader
import com.zaxxer.hikari.HikariDataSource
import org.skife.jdbi.v2.{DBI, Handle}

/**
  * Created by Riz
  */
object Ctx{

  lazy val dataSource : DataSource = {
    val ds = new HikariDataSource()
    ds.setDriverClassName("com.mysql.jdbc.Driver")
    ds.setUsername(props.getProperty("db.username"))
    ds.setPassword(props.getProperty("db.password"))
    ds.setJdbcUrl(props.getProperty("db.url"))
    ds
  }

  lazy val props : Properties = {
    val properties =  new Properties()
    properties.load(new FileInputStream("config/application.properties"))
    properties
  }

  lazy val dbi : DBI ={
    new DBI(dataSource)
  }

  lazy val pebbleEngine: PebbleEngine ={
    val loader = new ClasspathLoader()
    loader.setPrefix("view")
    loader.setSuffix(".peb")
    loader.setCharset(StandardCharsets.UTF_8.name())
    val engine = new PebbleEngine(loader)
    engine.setTemplateCache(null) //disable cache for dev
    engine
  }

  def view(view: String, model: java.util.Map[String, AnyRef] = new util.HashMap[String, AnyRef]()) : String= {
    val template = pebbleEngine.getTemplate(view)
    val writer = new StringWriter()
    template.evaluate(writer, model)
    return writer.toString
  }


  implicit class DBIForScala(x: DBI) {

    def run[T](f: (Handle) => T): T ={
      val handle = x.open()
      try{
        return f(handle)
      }
      finally{
        handle.close()
      }
    }
  }
}
