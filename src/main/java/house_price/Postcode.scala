package house_price

import java.io.{BufferedReader, InputStreamReader}
import java.time.format.DateTimeFormatter

import config.Ctx
import Ctx._
import org.apache.commons.csv.{CSVFormat, CSVParser}
import org.apache.http.client.fluent.Request
import org.skife.jdbi.v2.util.{IntegerColumnMapper, StringColumnMapper}

import scala.collection.JavaConversions._
/*
  * Created by Riz
  */
object Postcode {

  val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")

  /**
    * Fetch all postcodes for area and write them to db
    */
  def fetchAllPostCodeAreas() : Unit = {
    val recordItr = fetchCsv("http://opendatacommunities.org/sparql.csv?query=select+distinct+%3Fpostcodeareas%0D%0Awhere%0D%0A%7B%0D%0A++%3Fx+%3Chttp%3A%2F%2Fdata.ordnancesurvey.co.uk%2Fontology%2Fspatialrelations%2Fwithin%3E+%3Fy+.%0D%0A++%3Fy+a+%3Chttp%3A%2F%2Fdata.ordnancesurvey.co.uk%2Fontology%2Fpostcode%2FPostcodeArea%3E+.%0D%0A++%3Fy+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23label%3E+%3Fpostcodeareas.%0D%0A%7D")

    Ctx.dbi.run(h => {
      recordItr.foreach(record => {
        h.createStatement("Insert into postcode_area(area_code) VALUES (:code)")
          .bind("code", record.get(0))
          .execute()
      })
    })
  }

  /**
    * Fetch csv
    *
    * @param url url for csv
    * @return CSVParser containing the parsed csv
    */
  def fetchCsv(url: String): CSVParser = {
    val resp = Request.Get(url).execute().returnResponse()
    val inputStream = resp.getEntity().getContent()
    CSVFormat.EXCEL.withFirstRecordAsHeader().parse(new BufferedReader(new InputStreamReader(inputStream)))
  }

  /**
    * Count number of postcodes in a post code area
    *
    * @param postcodeArea The post code area
    * @return count of postcodes in area
    */
  def countPostCodesInArea(postcodeArea: String): Int ={
    val postCodeCountQuery = s"http://opendatacommunities.org/sparql.csv?query=select+%28COUNT%28DISTINCT+%3Fpostcode%29+AS+%3Fcount%29+%0D%0Awhere%0D%0A%7B%0D%0A++%3Fx+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23label%3E+%3Fpostcode+.%0D%0A++%3Fx+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E+%3Flat+.%0D%0A++%3Fx+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Flong+.%0D%0A++%3Fx+%3Chttp%3A%2F%2Fdata.ordnancesurvey.co.uk%2Fontology%2Fspatialrelations%2Fwithin%3E+%0D%0A+++++%3Chttp%3A%2F%2Fdata.ordnancesurvey.co.uk%2Fid%2Fpostcodearea%2F${postcodeArea}%3E+.%0D%0A%7D"
    val parser = fetchCsv(postCodeCountQuery)
    return parser.iterator().next().get(0).toInt
  }

  /**
    * Ftech all post codes within an area
    *
    * @param postcodeArea post code area
    */
  def fetchPostCodesForArea(postcodeArea: String): Unit = {
    val totalPostCodes = countPostCodesInArea(postcodeArea)
    Ctx.dbi.run(h => {
      val batch = h.prepareBatch("Insert into postcode(postcode, lat, lng) values (:postcode, :lat, :lng)")
      val limit = 10000
      for (offset <- Range(0, totalPostCodes, limit)) {
        val postCodeQuery = s"http://opendatacommunities.org/sparql.csv?query=select+%3Fpostcode+%3Flat+%3Flong%0D%0Awhere%0D%0A%7B%0D%0A++%3Fx+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23label%3E+%3Fpostcode+.%0D%0A++%3Fx+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E+%3Flat+.%0D%0A++%3Fx+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Flong+.%0D%0A++%3Fx+%3Chttp%3A%2F%2Fdata.ordnancesurvey.co.uk%2Fontology%2Fspatialrelations%2Fwithin%3E+%0D%0A+++++%3Chttp%3A%2F%2Fdata.ordnancesurvey.co.uk%2Fid%2Fpostcodearea%2F${postcodeArea}%3E+.%0D%0A%7D%0D%0Aoffset+${offset}%0D%0Alimit+${limit}"

        val csv = fetchCsv(postCodeQuery)

        csv.foreach(line => {
          val postcode = line.get(0)
          val lat = line.get(1).toDouble
          val lng = line.get(2).toDouble

          batch.add()
            .bind("house_price", postcode)
            .bind("lat", lat)
            .bind("lng", lng)
        })

        batch.execute()
      }
    })
  }

  /**
    * Fetch all post codes for post codes defined in post code areas,
    * updating fetched status after fetch
    */
  def fetchAllPostcodes(): Unit ={
      Ctx.dbi.run(h => {
      //get list of unaffected area codes
      val areaCodes = h.createQuery("SELECT area_code from postcode_area where fetched = 0")
                       .map(StringColumnMapper.INSTANCE)
                       .list()

      areaCodes.foreach(areaCode => {
        fetchPostCodesForArea(areaCode)

        //set postcode area fetched
        h.createStatement("UPDATE postcode_area set fetched=1 where area_code = :area_code")
          .bind("area_code", areaCode)
          .execute()
      })
    })
  }

  def checkPostcodeExists(postcode: String): Boolean ={
    Ctx.dbi.run(h =>
      h.createQuery("SELECT count(*) from postcode where postcode = :postcode")
         .bind("postcode", postcode)
         .map(IntegerColumnMapper.PRIMITIVE)
        .first() > 0
    )
  }
}