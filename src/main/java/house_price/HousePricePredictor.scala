package house_price

import java.sql.ResultSet
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util._

import config.Ctx
import Ctx._
import house_price.HousePricePredictor.PricePaid
import org.apache.commons.lang3.StringUtils
import org.skife.jdbi.v2.StatementContext
import org.skife.jdbi.v2.tweak.ResultSetMapper
import org.skife.jdbi.v2.util.{DoubleColumnMapper, StringColumnMapper}

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
/**
  * Created by Riz
  */
case class HousePriceRequest(postcode: String)
case class HousePriceResponse(
     @BeanProperty estimatedPrice:Int,
     @BeanProperty pricesPaid: List[PricePaid] )

object HousePricePredictor {
  val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")

  def predictHousePrice(req: HousePriceRequest) : HousePriceResponse = {
    val nearbyPostCodes = getNearbyPostCodes(req)
    val paidPrices      = getPaidPrices(nearbyPostCodes)

    paidPrices.foreach(paidPrice => {
      paidPrice.adjustedPrice = calculateInflationAdjustedPrice(paidPrice.date, paidPrice.price).toInt
    })

    val adjustedPrices = paidPrices.map(_.adjustedPrice)
    val (mean, sd) = calculateMeanAndStandardDeviation(adjustedPrices)
    val upperBound = mean + sd
    val lowerBound = mean - sd

    val filteredPaidPrice = paidPrices.filter(paidPrice => paidPrice.adjustedPrice >= lowerBound && paidPrice.adjustedPrice <= upperBound)

    val estimate = filteredPaidPrice.map(_.adjustedPrice)
                                      .sum / filteredPaidPrice.size

    HousePriceResponse(estimatedPrice = estimate.toInt, pricesPaid = filteredPaidPrice)
  }

  def getNearbyPostCodes(req: HousePriceRequest): List[String] = {
    Ctx.dbi.run(handle =>
      return handle.createQuery(
              "select code.postcode from postcode code, (select * from postcode where postcode = :postCode) as target " +
              "where code.lat < target.lat + 0.002 " +
              "and code.lat > target.lat - 0.002 " +
              "and code.lng < target.lng + 0.002 " +
              "and code.lng > target.lng - 0.002 " +
              "ORDER BY code.lng asc " +
              "LIMIT 100 ")
              .bind("postCode", req.postcode)
             // .bind("property_type", req.property_type)
              .map(StringColumnMapper.INSTANCE)
              .list()
    )
  }

  case class PricePaid(price: Int = -1, date:String = "" , paon: String = "", street: String = "", postcode:String = "",  var adjustedPrice:Int = -1 ) extends ResultSetMapper[PricePaid]{
    override def map(index: Int, rs: ResultSet, ctx: StatementContext): PricePaid = {
      new PricePaid(
        price    = rs.getInt("price"),
        date     = rs.getString("date"),
        paon     = rs.getString("paon"),
        street   = rs.getString("street"),
        postcode = rs.getString("postcode")
      )
    }
  }

  def getPaidPrices(postCodes: List[String]) : List[PricePaid] = {
    val paidPrices = new ArrayList[PricePaid]()

    for(postcode <- postCodes){
      paidPrices.addAll(getPricePaidForPostcode(postcode))
    }

    return paidPrices
  }

  def getPricePaidForPostcode(postCode: String): List[PricePaid] = {
    Ctx.dbi.run(handle =>
      handle.createQuery("""
          SELECT price, date, paon, street, postcode
          FROM price_paid
          WHERE postcode = :postCode""")
        .bind("postCode", postCode)
        .map(new PricePaid())
        .list()
    )
  }


  /**
    * Get sum of inflation
    *
    * @param startIndex start index (format: yyyymm)
    * @param endIndex end index (format: yyyymm)
    * @return the cummulative sum
    */
  def getCummulativeInflation(startIndex: Int, endIndex: Int) : Double = {
    Ctx.dbi.run(handle => {
      val inflation = handle.createQuery("""SELECT SUM(inflation) FROM monthly_inflation
                                             WHERE month >= :startIndex
                                             AND month <= :endIndex
                                         """.stripMargin)
        .bind("startIndex", startIndex)
        .bind("endIndex", endIndex)
        .map(DoubleColumnMapper.PRIMITIVE)
        .first()

      return inflation / 100
    })
  }

  /**
    * converts a local date to an index in monthly inflation price index
    *
    * @param date the date
    * @return index
    */
  def dateToIndex(date: LocalDate) : Int ={
    val monthStr = StringUtils.leftPad(s"${date.getMonthValue}",2, "0")
    s"${date.getYear}${monthStr}".toInt
  }

  /**
    * Calculate inflation adjusted price
    *
    * @param startDate date the house was sold
    * @param price price when the house was last sold
    * @return inflation adjusted price
    */
  def calculateInflationAdjustedPrice(startDate: String, price: Int): Double = {
    val start      = LocalDate.parse(startDate, dateFormatter)
    val startIdx   = dateToIndex(start)
    val currentIdx = dateToIndex(LocalDate.now())
    val inflation  = getCummulativeInflation(startIdx, currentIdx)

    val newPrice = inflation match{
      case 0 => price                                //if inflation is 0
      case i if i > 0 => price + (price * inflation) //if inflation is > 0
      case i if i < 0 => price - (price * inflation) //if inflation < 0
    }
    newPrice
  }

  def calculateMeanAndStandardDeviation(seq: Seq[Int]): (Double, Double) ={
    val size = seq.size
    val mean = seq.sum / size
    val variance = seq.map(x => Math.pow((x - mean), 2)) //each val - mean
                      .sum /seq.size                     //sum / size

    (mean, Math.sqrt(variance))
  }
}
