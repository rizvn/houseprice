package house_price

import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import java.time.YearMonth
import java.time.format.DateTimeFormatter

import org.apache.commons.csv.{CSVFormat, CSVParser}
import org.apache.commons.lang3.StringUtils

import scala.collection.JavaConversions._

/**
  * Created by Riz
  */
object MonthlyInflationParser {

  val formatter = DateTimeFormatter.ofPattern("MMM-yy")

  def parseFile(): Unit ={
    val writer = Files.newBufferedWriter(Paths.get("src/main/resources/monthly_fixed.csv"), StandardCharsets.UTF_8)
    val parser = CSVParser.parse(new File("src/main/resources/monthly_inflation.csv"), StandardCharsets.UTF_8, CSVFormat.DEFAULT)

    parser.foreach(item => {
      val dateStr = item.get(0)
      val yearOfMonth = YearMonth.parse(dateStr, formatter)
      val inflation = item.get(1).replace("%", "").toDouble

      val year = if (yearOfMonth.getYear > 2050) yearOfMonth.getYear  - 100 else yearOfMonth.getYear
      val month = StringUtils.leftPad(yearOfMonth.getMonthValue+"", 2, "0")
      val dateIndex = year + month
      print(dateIndex)
      writer.write(s"${dateIndex},${inflation}\n")
    })
    writer.close()
  }
}


