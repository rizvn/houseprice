create database postcode_db;

CREATE TABLE postcode_db.postcode_area
(
    area_code VARCHAR(10) NOT NULL PRIMARY KEY ,
    fetched BIT NOT NULL DEFAULT 0
);

CREATE TABLE postcode_db.postcode
(
    postcode VARCHAR(10) NOT NULL PRIMARY KEY ,
    lat DECIMAL(9,6) NOT NULL,
    lng DECIMAL(9,6) NOT NULL
);

CREATE TABLE postcode_db.price_paid
(
    id VARCHAR(38) NOT NULL PRIMARY KEY,
    price INT,
    date DATETIME,
    postcode VARCHAR(10),
    property_type VARCHAR(1),
    build_type VARCHAR(1),
    duration VARCHAR(1),
    paon VARCHAR(50),
    saon VARCHAR(50),
    street VARCHAR(100),
    locality VARCHAR(50),
    town_city VARCHAR(50),
    district VARCHAR(50),
    county VARCHAR(50),
    ppd_category VARCHAR(1),
    record_status VARCHAR(1)
);
-- create index on post code
CREATE INDEX price_paid_postcode_index ON postcode_db.price_paid (postcode);

CREATE TABLE postcode_db.monthly_inflation
(
    month INT PRIMARY KEY NOT NULL,
    inflation DECIMAL(4,2)
);

-- after loading monthly data
CREATE UNIQUE INDEX monthly_inflation_month_uindex ON postcode_db.monthly_inflation (month);

-- after loading price_paid data
ALTER TABLE postcode_db.price_paid ADD price_adjusted INT NULL;
ALTER TABLE price_paid add column pk INT NOT NULL AUTO_INCREMENT FIRST, ADD primary KEY Id(pk)
