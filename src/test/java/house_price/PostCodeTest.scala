package house_price

import org.junit.{Assert, Ignore, Test}

/**
  * Created by Riz
  */
class PostCodeTest{

  @Test
  @Ignore
  def testFetchAllPostCodeAreas(): Unit ={
    Postcode.fetchAllPostCodeAreas()
  }

  @Test
  @Ignore
  def testCountPostCodesInArea(): Unit ={
    val count =  Postcode.countPostCodesInArea("BB")
    Assert.assertTrue(count > 13000)
  }

  @Test
  @Ignore
  def testFetchAllPostCodes(): Unit ={
    Postcode.fetchAllPostcodes()
  }

  @Test
  @Ignore
  def testCheckPostCodeExists(): Unit ={
    Assert.assertTrue(Postcode.checkPostcodeExists("BB1 5PW"))
    Assert.assertFalse(Postcode.checkPostcodeExists("ABC123"))
  }

}
