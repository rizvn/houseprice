package house_price

import org.junit.{Assert, Ignore, Test}

/**
  * Created by Riz
  */
class HousePricePredictorTest {

  val sut = HousePricePredictor

  @Test
  def predictHousePrice(): Unit ={
    val nearbyPostCodes  = sut.getNearbyPostCodes(HousePriceRequest(postcode="BB1 5PW"))
    Assert.assertFalse(nearbyPostCodes.isEmpty)
  }

  @Test
  def getPricePaidForPostcode(): Unit ={
    val pricesPaid  = sut.getPricePaidForPostcode("BB1 5PW")
    Assert.assertFalse(pricesPaid.isEmpty)
  }

  @Test
  def testCalculateInflationAdjustedPrice(): Unit ={
    val result = sut.calculateInflationAdjustedPrice("1995-12-18 00:00:00.0", 1)
    Assert.assertNotNull(result)
  }

  @Test
  def testPredictHousePrice(): Unit ={
    val result = sut.predictHousePrice(HousePriceRequest(postcode = "BB1 8RL"))
    Assert.assertNotNull(result)
  }

  @Test
  def testCalculateStandardDeviation() : Unit = {
    val (mean, sd) = sut.calculateMeanAndStandardDeviation(Seq(600, 470 , 170 , 430, 300))
    Assert.assertTrue(394 == mean.toInt)
    Assert.assertTrue(147 == sd.toInt)
  }
}
